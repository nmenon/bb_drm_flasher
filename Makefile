CFLAGS = -O3 -L/usr/local/lib -I/usr/local/include -ltfb --static
all: beagle_broadcast beagle_cyclon beagle_splash
	@strip beagle_broadcast beagle_cyclon beagle_splash
	@echo "Done"

clean:
	@rm -f beagle_broadcast beagle_cyclon beagle_splash

beagle_splash: beagle_splash.c
	@$(CC) -o beagle_splash beagle_splash.c $(CFLAGS)

beagle_broadcast: beagle_broadcast.c
	@$(CC) -o beagle_broadcast beagle_broadcast.c $(CFLAGS)

beagle_cyclon: beagle_cyclon.c
	@$(CC) -o beagle_cyclon beagle_cyclon.c $(CFLAGS)
