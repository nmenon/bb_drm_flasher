#!/bin/bash

if [ ! -e /dev/fb0 ]; then
	echo "Framebuffer interface is missing. exit"
	exit 1
fi

echo "Lets switch off lightdm if that is running."
sudo service lightdm stop

./beagle_splash "Flashing from X to Y"

for j in $(seq 1 10)
do
	for i in $(seq 1 6)
       	do
		./beagle_cyclon $i
	       	./beagle_broadcast 10 "hello $i message $j"
       	done
done
