/* SPDX-License-Identifier: BSD-3-Clause */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#include <tfblib/tfblib.h>
#include <tfblib/tfb_colors.h>

#define BEAGLE_MODEL_FILE "/proc/device-tree/model"
#define BEAGLE_DEFAULT_BANNER "BeagleBoard.org"

struct linux_logo {
	unsigned int width;
	unsigned int height;
	unsigned int clutsize;	/* LINUX_LOGO_CLUT224 only */
	const unsigned char *clut;	/* LINUX_LOGO_CLUT224 only */
	const unsigned char *data;
};

/**
 * STEPS TO CREATE A LOGO
 * https://variwiki.com/index.php?title=Yocto_Linux_logo
 * Create a png file with max length of 400 pixels (but make it a square image)
 * $ sudo apt-get install netpbm
 * $ pngtopnm logo.png | ppmquant 224 | pnmnoraw > logo.ppm
 *
 * Use the file linux/drivers/video/logo/pnmtologo to convert the ppm file to logo
 * /build/linux/drivers/video/logo/pnmtologo -n logo_b -o logo.h -t clut224 logo.ppm
 *
 * Edit logo.h to
 * a) drop linux_logo include header
 * b) drop __initdata (2 places) and __initconst (1 place), drop .type (1 place)
 * c) Add: static const struct linux_logo *logo = &logo_b; at the very end
 */
//#include "logo_boris_tux.h"
#include "logo_beagleboard.h"

static void fb_show_logo(uint32_t start_x, uint32_t start_y)
{
	uint32_t x, y;
	uint32_t color;

	for (y = 0; y < logo->height; y++) {
		for (x = 0; x < logo->width; x++) {
			int clut_index;
			int clut_r;
			int clut_g;
			int clut_b;

			clut_index = logo->data[(y * logo->height) + x];

			clut_r = (clut_index - 32) * 3;
			clut_g = clut_r + 1;
			clut_b = clut_g + 1;

			color =
			    tfb_make_color(logo->clut[clut_r], logo->clut[clut_g],
					   logo->clut[clut_b]);
			tfb_draw_pixel(start_x + x, start_y + y, color);
		}
	}
}

bool font_iter_cb_select_font16(struct tfb_font_info *fi, void *user_arg)
{
	if (fi->height == 16) {

		int rc = tfb_set_current_font(fi->font_id);

		if (rc != TFB_SUCCESS) {
			fprintf(stderr, "tfb_set_current_font() failed: %s\n",
				tfb_strerror(rc));
			abort();
		}

		return false;	/* stop iterating over fonts */
	}

	return true;
}

int draw_banner(void)
{
	char *file_name = BEAGLE_MODEL_FILE;
	FILE *fp;
	int read;
	size_t len;
	char *line = NULL, *display_string = BEAGLE_DEFAULT_BANNER;
	uint32_t max_x = tfb_screen_width();
	uint32_t max_y = tfb_screen_height();

	if (logo->width > max_x) {
		printf("logo width = %d, max_x = %d! Sorry too big\n", logo->width,
		       max_x);
		return 1;
	}
	if (logo->height > max_y) {
		printf("logo height = %d, max_y = %d! Sorry too big\n", logo->height,
		       max_y);
		return 1;
	}

	fp = fopen(file_name, "r");
	if (fp == NULL)
		goto display_banner;

	while ((read = getline(&line, &len, fp)) != -1) ;
	fclose(fp);

display_banner:
	tfb_clear_screen(tfb_black);
	tfb_set_font_by_size(16, 32);

	if (line)
		display_string = line;

	tfb_draw_string_scaled(20, max_y - (tfb_get_curr_font_height() * 4),
			       tfb_orange, tfb_black, 2, 2, display_string);

	if (line)
		free(line);

	fb_show_logo(max_x - (logo->width + 10), max_y - (logo->height + 10));

	return 0;
}

int main(int argc, char **argv)
{
	int rc;

	/*
	 * Set a font after searching for the best fit with a custom callback.
	 * If the criteria is just the size as in this case, the more convenient
	 * utility function tfb_set_font_by_size() can be used.
	 */
	tfb_iterate_over_fonts(font_iter_cb_select_font16, NULL);
	rc = tfb_acquire_fb(TFB_FL_NO_TTY_KD_GRAPHICS, NULL, NULL);

	if (rc != TFB_SUCCESS) {
		fprintf(stderr, "tfb_acquire_fb() failed: %s\n", tfb_strerror(rc));
		return 1;
	}

	draw_banner();

	if (argc == 2) {
		tfb_draw_string_scaled(20,
				       tfb_screen_height() -
				       (tfb_get_curr_font_height() * 8), tfb_red,
				       tfb_black, 1, 1, argv[1]);
	}
	tfb_release_fb();
	return 0;
}
