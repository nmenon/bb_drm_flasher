/* SPDX-License-Identifier: BSD-3-Clause */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#include <tfblib/tfblib.h>
#include <tfblib/tfb_colors.h>

#define MAX_STATE 6
#define HEIGHT_STATUS 40

static inline double rad_to_deg(double rad)
{
	return (rad / (2 * M_PI)) * 360.0;
}

static void draw_cyclone(int state)
{
	uint32_t w = tfb_screen_width();
	uint32_t h = tfb_screen_height();
	uint32_t h_s = (h / 2) - HEIGHT_STATUS;
	uint32_t w_chunks = w / MAX_STATE;

	/* status area border */
	tfb_fill_rect(0, h_s, w, HEIGHT_STATUS, tfb_black);
	tfb_draw_rect(0, h_s, w, HEIGHT_STATUS, tfb_yellow);

	/* Draw status circle */
	tfb_fill_circle(w_chunks * state - HEIGHT_STATUS, h_s + (HEIGHT_STATUS / 2),
			HEIGHT_STATUS / 2, tfb_red);

}

int main(int argc, char **argv)
{
	int rc;
	int state;
	if (argc <= 1) {
		printf("Usage: %s state \n", argv[0]);
		exit(1);
	}

	state = atoi(argv[1]);

	if (state < 1 || state > MAX_STATE) {
		printf("%s: state %d not between 1 and 6 \n", argv[0], state);
		exit(1);
	}

	rc = tfb_acquire_fb(TFB_FL_NO_TTY_KD_GRAPHICS, NULL, NULL);

	if (rc != TFB_SUCCESS) {
		fprintf(stderr, "tfb_acquire_fb() failed: %s\n", tfb_strerror(rc));
		return 1;
	}

	draw_cyclone(state);
	tfb_release_fb();
	return 0;
}
