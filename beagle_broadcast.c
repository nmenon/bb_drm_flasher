/* SPDX-License-Identifier: BSD-3-Clause */

#include <stdio.h>
#include <stdlib.h>
#include <tfblib/tfblib.h>
#include <tfblib/tfb_colors.h>

#define BEAGLE_BROADCAST_FILE "/tmp/bb.org_broadcast"
#define START_X 20
#define START_Y 20

bool font_iter_cb_select_font16(struct tfb_font_info *fi, void *user_arg)
{
	if (fi->height == 16) {

		int rc = tfb_set_current_font(fi->font_id);

		if (rc != TFB_SUCCESS) {
			fprintf(stderr, "tfb_set_current_font() failed: %s\n",
				tfb_strerror(rc));
			abort();
		}

		return false;	/* stop iterating over fonts */
	}

	return true;
}

int append_to_log(int num_lines, char *message_to_broadcast)
{
	char *file_name = BEAGLE_BROADCAST_FILE;
	char *file_name_tmp = BEAGLE_BROADCAST_FILE ".tmp";
	FILE *fp, *fpt;
	int line_count = 0, skip_lines = 0;
	int read;
	size_t len;
	char *line = NULL;

	fpt = fopen(file_name_tmp, "w");
	if (fpt == NULL)
		return (EXIT_FAILURE);

	fp = fopen(file_name, "r");
	if (fp == NULL)
		goto write_final_message;

	while ((read = getline(&line, &len, fp)) != -1)
		line_count++;

	if (line_count < num_lines)
		skip_lines = 0;
	else
		skip_lines = line_count - num_lines + 1;

	line_count = 0;
	/* Reset pointer to top */
	fseek(fp, 0, SEEK_SET);

	while ((read = getline(&line, &len, fp)) != -1) {
		line_count++;
		if (line_count <= skip_lines)
			continue;
		fputs(line, fpt);
	}

	if (line)
		free(line);

	fclose(fp);

write_final_message:
	fprintf(fpt, "%s\n", message_to_broadcast);

	fclose(fpt);

	remove(file_name);
	rename(file_name_tmp, file_name);

	return 0;
}

int display_log(int num_lines)
{
	char *file_name = BEAGLE_BROADCAST_FILE;
	FILE *fp;
	int line_y = START_Y;
	int read;
	size_t len;
	char *line = NULL;

	fp = fopen(file_name, "r");
	if (fp == NULL)
		return (EXIT_FAILURE);

	while ((read = getline(&line, &len, fp)) != -1) {
		tfb_draw_string(20, line_y, tfb_yellow, tfb_gray, line);
		line_y += tfb_get_curr_font_height();
	}

	if (line)
		free(line);

	fclose(fp);
	return 0;
}

void clear_area(int num_lines)
{
	uint32_t y = START_Y - 5;
	uint32_t y_end = num_lines * tfb_get_curr_font_height() + 5;
	uint32_t w = tfb_screen_width();

	/* Clear the area */
	tfb_fill_rect(0, y, w, y_end, tfb_black);
	/* Draw the border */
	tfb_draw_rect(0, y, w, y_end, tfb_red);
}

int main(int argc, char **argv)
{
	int rc;
	int num_lines;

	if (argc <= 2) {
		printf("Usage: %s num_lines_display message_to_display\n", argv[0]);
		exit(1);
	}

	num_lines = atoi(argv[1]);

	if (num_lines < 1 || num_lines > 10) {
		printf("%s: num_lines_display : %d: not between 1 and 10 \n", argv[0],
		       num_lines);
		exit(1);
	}

	rc = append_to_log(num_lines, argv[2]);
	if (rc) {
		printf("%s: rotated addition to log failed: %d\n", argv[0], rc);
		return rc;
	}

	/*
	 * Set a font after searching for the best fit with a custom callback.
	 * If the criteria is just the size as in this case, the more convenient
	 * utility function tfb_set_font_by_size() can be used.
	 */
	tfb_iterate_over_fonts(font_iter_cb_select_font16, NULL);

	rc = tfb_acquire_fb(TFB_FL_NO_TTY_KD_GRAPHICS, NULL, NULL);

	if (rc != TFB_SUCCESS) {
		fprintf(stderr, "tfb_acquire_fb() failed: %s\n", tfb_strerror(rc));
		tfb_release_fb();
		return 1;
	}

	clear_area(num_lines);

	display_log(num_lines);

	tfb_release_fb();

	return 0;
}
